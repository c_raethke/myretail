# README #

myRetailService is a RESTful web service to get and update product information, with endpoints:  
GET /products/{id}  
PUT /products/{id}

Example call: http://localhost:8080/product/13860428

MongoDB (port 27017) and Java 8 must be installed prior to running.

### Build ###
* ./mvnw clean package

### Test ###
* ./mvnw clean test

### Run ###
* Start database: mongod
* Start server: java -jar target/myRetailService-0.0.1-SNAPSHOT.jar
package com.myRetail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * Spring boot base class
 * @author Cody Raethke
 *
 */
@SpringBootApplication
public class MyRetailServiceApplication {

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	public static void main(final String[] args) {
		SpringApplication.run(MyRetailServiceApplication.class, args);
	}
}

package com.myRetail.webservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.myRetail.business.service.ProductService;
import com.myRetail.integration.vo.Product;

/**
 * Web service for accessing product information
 * @author Cody Raethke
 *
 */
@RestController
public class ProductWebService {

	@Autowired
	private ProductService productService;

	/**
	 * Get information about a product
	 * @param id The product id
	 * @return Product (converted to JSON by Spring)
	 */
	@GetMapping("/product/{id}")
	public Product getProduct(@PathVariable final int id) {
		return productService.getProductById(id);
	}

	/**
	 * Updates a product's price information
	 * @param product Product (converted from JSON by Spring)
	 */
	@PutMapping("/product/{id}")
	public void updateProduct(@RequestBody final Product product) {
		productService.saveProductPrice(product);
	}
}

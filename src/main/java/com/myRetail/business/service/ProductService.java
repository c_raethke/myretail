package com.myRetail.business.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.myRetail.integration.repository.PriceRepository;
import com.myRetail.integration.vo.Price;
import com.myRetail.integration.vo.Product;

/**
 * Service to access Product information
 * @author Cody Raethke
 *
 */
@Service
public class ProductService {

	@Autowired
	private PriceRepository priceRepository;

	@Autowired
	private RestTemplate restTemplate;

	@Value("${externalService.baseUrl}")
	private String baseUrl;

	@Value("${externalService.params}")
	private String params;

	/**
	 * Get a product by its ID
	 * @param id The ID of the product
	 * @return Product with information from both database and webservice
	 */
	public Product getProductById(final int id) {
		JsonNode json = restTemplate.getForObject(baseUrl + id + params, JsonNode.class);
		String title = json.get("product").get("item").get("product_description").get("title").asText();
		return new Product(id, title, priceRepository.findOne(id));
	}

	/**
	 * Saves product information to the database
	 * @param product Product to save information for
	 */
	public void saveProductPrice(final Product product) {
		Price productPrice = product.getCurrentPrice();
		productPrice.setItemId(product.getId());
		priceRepository.save(productPrice);
	}
}

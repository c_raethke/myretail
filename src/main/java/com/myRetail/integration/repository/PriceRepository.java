package com.myRetail.integration.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.myRetail.integration.vo.Price;

/**
 * Repository to access Product information in MongoDB
 * @author Cody Raethke
 *
 */
public interface PriceRepository extends MongoRepository<Price, Integer> {

	// Implementation generated at runtime by Spring Data

}

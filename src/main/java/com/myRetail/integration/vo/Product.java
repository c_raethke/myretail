package com.myRetail.integration.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Value object representing a myRetail Product
 * @author Cody Raethke
 *
 */
public class Product {

	private int id;

	private String name;

	@JsonProperty("current_price")
	private Price currentPrice;

	public Product() {}

	public Product(final int id, final String name, final Price price) {
		this.id = id;
		this.name = name;
		currentPrice = price;
	}

	public int getId() {
		return id;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public Price getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(final Price price) {
		currentPrice = price;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((currentPrice == null) ? 0 : currentPrice.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (currentPrice == null) {
			if (other.currentPrice != null)
				return false;
		} else if (!currentPrice.equals(other.currentPrice))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", price=" + currentPrice + "]";
	}
}

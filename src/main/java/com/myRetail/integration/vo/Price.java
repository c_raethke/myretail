package com.myRetail.integration.vo;

import java.math.BigDecimal;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Value object representing a price in a specific currency
 * @author Cody Raethke
 *
 */
public class Price {

	@Id
	@JsonIgnore
	private Integer itemId;

	private BigDecimal value;

	@JsonProperty("currency_code")
	private String currencyCode;

	public Price() { }

	public Price(final Integer itemId, final BigDecimal value, final String currencyCode) {
		this.itemId = itemId;
		this.value = value;
		this.currencyCode = currencyCode;
	}

	public Price(final BigDecimal value, final String currencyCode) {
		this.value = value;
		this.currencyCode = currencyCode;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(final BigDecimal value) {
		this.value = value;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(final String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public Integer getItemId() {
		return itemId;
	}

	public void setItemId(final Integer itemId) {
		this.itemId = itemId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((currencyCode == null) ? 0 : currencyCode.hashCode());
		result = prime * result + ((itemId == null) ? 0 : itemId.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Price other = (Price) obj;
		if (currencyCode == null) {
			if (other.currencyCode != null)
				return false;
		} else if (!currencyCode.equals(other.currencyCode))
			return false;
		if (itemId == null) {
			if (other.itemId != null)
				return false;
		} else if (!itemId.equals(other.itemId))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Price [itemId=" + itemId + ", value=" + value + ", currencyCode=" + currencyCode + "]";
	}
}

package com.myRetail.integration.vo

/**
 * Define instances of {@link Product} to use for testing
 * @author Cody Raethke
 *
 */
class Products {

	def static getBasicProduct() {
		new Product(1, "Test Product", Prices.getBasicPrice())
	}
}

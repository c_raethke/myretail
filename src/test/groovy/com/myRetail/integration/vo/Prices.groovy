package com.myRetail.integration.vo

/**
 * Define instances of {@link Price} to use for testing
 * @author Cody Raethke
 *
 */
class Prices {

	def static getBasicPrice() {
		new Price(9.99, "USD")
	}

	def static getBasicPriceWithId() {
		new Price(1, 9.99, "USD")
	}
}

package com.myRetail.integration.repository

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest
import org.springframework.data.mongodb.core.MongoTemplate

import com.myRetail.integration.vo.Price
import com.myRetail.integration.vo.Prices

import spock.lang.Specification

@DataMongoTest
class PriceRepositorySpec extends Specification {

	@Autowired
	MongoTemplate mongoTemplate

	@Autowired
	PriceRepository priceRepository

	def "should get price"() {
		given:
		def price = Prices.basicPriceWithId

		when:
		mongoTemplate.save(price)
		def savedPrice = priceRepository.findOne(1)

		then:
		savedPrice == Prices.basicPriceWithId
	}

	def "should save price"() {
		given:
		def price = Prices.basicPriceWithId

		when:
		priceRepository.save(price)
		def savedPrice = mongoTemplate.findById(1, Price.class)

		then:
		savedPrice == Prices.basicPriceWithId
	}
}

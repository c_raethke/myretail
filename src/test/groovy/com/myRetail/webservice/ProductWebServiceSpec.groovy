package com.myRetail.webservice

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*

import org.springframework.http.MediaType

import com.fasterxml.jackson.databind.ObjectMapper
import com.myRetail.business.service.ProductService
import com.myRetail.integration.vo.Product
import com.myRetail.integration.vo.Products

import groovy.json.*
import spock.lang.Specification

class ProductWebServiceSpec extends Specification {

	def productWebService = new ProductWebService()

	def mockMvc = standaloneSetup(productWebService).build()

	ProductService productService = Mock()

	def objectMapper = new ObjectMapper()

	def setup() {
		productWebService.productService = productService
	}

	def "should get product information"() {
		given:
		def product = Products.basicProduct

		when:
		def request = mockMvc.perform(get("/product/1"))

		then:
		1 * productService.getProductById(1) >> product
		def result = request.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
			.andReturn()

		def json = objectMapper.readValue(result.getResponse().getContentAsString(), Product.class)
		json == product
	}

	def "should save price information"() {
		given:
		def product = Products.basicProduct

		when:
		def result = mockMvc.perform(put("/product/1")
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.content(objectMapper.writeValueAsString(product)))

		then:
		1 * productService.saveProductPrice({ Product p -> p == product })
		result.andExpect(status().isOk())
	}
}

package com.myRetail.business.service

import static org.springframework.test.web.client.match.MockRestRequestMatchers.*
import static org.springframework.test.web.client.response.MockRestResponseCreators.*

import org.springframework.http.MediaType
import org.springframework.test.web.client.MockRestServiceServer
import org.springframework.web.client.RestTemplate

import com.myRetail.integration.repository.PriceRepository
import com.myRetail.integration.vo.Price
import com.myRetail.integration.vo.Products

import groovy.json.JsonBuilder
import spock.lang.Specification

class ProductServiceSpec extends Specification {

	def productService = new ProductService()

	def restTemplate = new RestTemplate()

	MockRestServiceServer server

	PriceRepository priceRepository = Mock()

	def setup() {
		server = MockRestServiceServer.createServer(restTemplate)
		productService.restTemplate = restTemplate
		productService.priceRepository = priceRepository
		productService.baseUrl = "http://www.test.com/"
		productService.params = ""
	}

	def "should get product details from service and repository"() {
		given:
		def builder = new JsonBuilder()
		builder.product {
			item {
				product_description {
					title "Test Product"
				}
			}
		}
		def price = new Price(currencyCode: "USD", value: 9.99)

		when:
		server.expect(requestTo(productService.baseUrl + 1 + productService.params))
			.andRespond(withSuccess(builder.toPrettyString(), MediaType.APPLICATION_JSON))
		def resultProduct = productService.getProductById(1)

		then:
		1 * priceRepository.findOne(1) >> price
		resultProduct == Products.basicProduct
	}

	def "should save product price"() {
		given:
		def product = Products.basicProduct

		when:
		productService.saveProductPrice(product)

		then:
		1 * priceRepository.save(product.currentPrice) >> product.currentPrice
	}
}
